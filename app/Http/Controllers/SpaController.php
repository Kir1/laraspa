<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Genre;
use Illuminate\Support\Facades\Log;

class SpaController extends Controller
{
    public function index(Request $request)
    {
        return view('index');
    }

    public function editBook(Request $request,$id)
    {
        $book = Book::where('id', '=', $id)->select('user_id')->first();

        if ($book) {
            if (Auth::user()->id == $book->user_id) return view('index');
            return abort(403);
        }

        return abort(404);
    }

    public function getBook(Request $request, $id)
    {
        $book = Book::where('id', '=', $id)->first();

        if ($book) {
            if (Auth::user()->id == $book->user_id) return response()->json(['book' => $book->toJson(), 'genres' => Genre::all()->toJson()]);
            return response()->json(['errors' => [['error' => 'У вас нет прав на редактирование этой книги.']]]);
        }

        return response()->json(['errors' => [['error' => 'Данной книги больше не существует11.']]]);
    }
}
