<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAnonimus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $userAnomin = (Auth::guard($guard)->check() == false);

        if ($userAnomin) return $next($request);

        if ($request->method() == 'GET')
            abort(403);
        else
            return response()->json(['errors' => [['error' => 'Данное действие доступно только неавторизированным пользователям. Скорее всего Вы уже авторизированы, перезайдите на сайт.']]]);
    }
}
