<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $userAuth = Auth::guard($guard)->check();

        if ($userAuth) return $next($request);

        if ($request->method() == 'GET')
            abort(403);
        else
            return response()->json(['errors' => [['error' => 'Вы должны авторизироваться для совершения данного действия. Возможно, Ваш аккаунт заблокирован.']]]);
    }
}
