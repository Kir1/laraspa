function pathPublicImage(path) {
    if (path.indexOf('blob') + 1) return path;
    return '/images/' + path;
}

export {pathPublicImage}