import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import BootstrapVue from 'bootstrap-vue';
import App from './components/App';
import AuthPage from './modules/user/pages/AuthPage';
import RegisterPage from './modules/user/pages/RegisterPage';
import MainPage from './pages/MainPage';
import EditBookPage from './modules/book/pages/EditBookPage.vue';
import CreateBookPage from './modules/book/pages/CreateBookPage.vue';

Vue.use(VueRouter);

Vue.use(Vuex);

Vue.use(BootstrapVue);

function ini_store() {
    return {
        user: null,
        books: [],
        preloader: true
    }
}

const store = new Vuex.Store({
    state: ini_store(),
    mutations: {
        auth (state, user) {
            state.user = user;
        },
        logout (state) {
            state.user = null;
            state.books = null;
        },
        preloaderHide (state) {
            state.preloader = false;
        },
        books (state, books) {
            state.books = JSON.parse(books);
        }
    }
})

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'main',
            component: MainPage
        },
        {
            path: '/authorization',
            name: 'auth',
            component: AuthPage
        },
        {
            path: '/registration',
            name: 'register',
            component: RegisterPage
        },
        {
            name: 'createBook',
            path: '/books/create',
            component: CreateBookPage
        }, {
            name: 'editBook',
            path: '/books/:id/edit',
            component: EditBookPage
        }
    ]
});

const app = new Vue({
    el: '#app',
    components: {App},
    router,
    store
});