<?php

use Illuminate\Database\Seeder;
use App\Genre;

class Genres extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Genre::create(['title' => 'Комедия']);
        Genre::create(['title' => 'Ужасы']);
        Genre::create(['title' => 'Драма']);
    }
}
