<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(['name' => 'user', 'password' => Hash::make(123)]);
        User::create(['name' => 'user2', 'password' => Hash::make(123)]);
    }
}
