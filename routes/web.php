<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator as Validator;
use App\Book;
use App\User;
use App\Genre;


Route::post('get-user', function () {
    return response()->json([
        'user' => Auth::user()
    ]);
});

Route::group(['middleware' => ['check.anonimus']], function () {
    Route::post('register', function (Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|unique:users',
            'password' => 'required|min:3'
        ], [
            'name.unique' => 'Данный логин занят'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()]);
        }

        $user = User::create([
            'name' => $request->input('name'),
            'password' => Hash::make($request->input('password')),
        ]);

        Auth::login($user);

        return response()->json(['user' => $user]);
    });

    Route::post('auth', function (Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3',
            'password' => 'required|min:3'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()]);
        }

        $user = User::where('name', $request->input('name'))->first();

        if ($user && Hash::check($request->input('password'), $user->password)) {
            Auth::login($user);
            return response()->json(['user' => $user]);
        } else
            return response()->json(['errors' => [['error' => 'Ввведите правильный логин и пароль']]]);
    });

    Route::get('authorization', 'SpaController@index');
    Route::get('registration', 'SpaController@index');
});

Route::group(['middleware' => ['check.auth']], function () {
    Route::post('logout', function () {
        Auth::logout();
        return response()->json([
            'user' => null
        ]);
    });

    Route::post('genres', function (Request $request) {
        return response()->json(['genres' => Genre::all()->toJson()]);
    });

    Route::group(array('prefix' => 'books'), function () {
        Route::post('', function (Request $request) {
            $stringSearch = $request->input('pathString');
            if ($stringSearch)
                $books = Book::where('title', 'like', '%' . $stringSearch . '%')
                    ->orWhere('author', 'like', '%' . $stringSearch . '%')
                    ->orWhere('genre', 'like', '%' . $stringSearch . '%')
                    ->get()
                    ->toJson();
            else
                $books = Book::all()->toJson();

            return response()->json(['books' => $books]);
        });

        Route::post('create', function (Request $request) {
            $validator = Validator::make($request->all(), [
                'title' => 'required|max:100|string',
                'genre' => 'required|string',
                'author' => 'required|max:150|string|regex:/^\w{1,}\s\w{1,}\s\w{1,}$/iu',
                'cover' => 'required|image|max:200',
                'user_id' => 'required|numeric'
            ], [
                'author.regex' => 'В поле автора введите через пробел его имя, фамилию и отчество.',
                'cover.image' => 'Файл обложки должен быть картинкой.'
            ]);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()]);
            }

            $book = new Book;
            $pathImage = $request->file('cover')->store('covers', ['disk' => 'openFiles']);
            $book->cover = $pathImage;
            $book->user_id = $request->input('user_id');
            $book->title = $request->input('title');
            $book->genre = $request->input('genre');
            $book->author = $request->input('author');
            $book->save();

            return response()->json([
                'book' => $book->toJson()
            ]);
        });

        Route::post('{id}/update', function (Request $request) {
            $book = Book::find($request->input('id'));

            if ($book) {
                $validator = Validator::make($request->all(), [
                    'title' => 'required|max:100|string',
                    'genre' => 'required|string',
                    'author' => 'required|max:150|string|regex:/^\w{1,}\s\w{1,}\s\w{1,}$/iu'
                ], [
                    'author.regex' => 'В поле автора введите через пробел его имя, фамилию и отчество.',
                ]);

                if ($validator->fails()) {
                    return response()->json(['errors' => $validator->messages()]);
                }

                if ($request->file('cover')) {
                    $validator = Validator::make($request->all(), [
                        'cover' => 'image|max:200',
                    ], [
                        'cover.image' => 'Файл обложки должен быть картинкой.',
                    ]);

                    if ($validator->fails()) {
                        return response()->json(['errors' => $validator->messages()]);
                    }

                    if ($book->cover) Storage::disk('openFiles')->delete($book->cover);
                    $pathImage = $request->file('cover')->store('covers', ['disk' => 'openFiles']);
                    $book->cover = $pathImage;
                }
                $book->title = $request->input('title');
                $book->genre = $request->input('genre');
                $book->author = $request->input('author');
                $book->save();

                return response()->json([
                    'book' => $book->toJson()
                ]);
            }

            return response()->json(['errors' => [['error' => 'Данной книги больше не существует.']]]);
        });

        Route::get('create', 'SpaController@index');
        Route::get('{id}/edit', 'SpaController@editBook');

        Route::post('{id}', 'SpaController@getBook');

    });
});

Route::get('/', 'SpaController@index');